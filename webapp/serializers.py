from rest_framework import serializers
from webapp.models import Webapp, LANGUAGE_CHOICES, STYLE_CHOICES

class WebappSerializer(serializers.ModelSerializer):
	class Meta:
		model = Webapp
		fields = ('id','title','code','linenos','language','style')

	def create(self, validated_data):
		'''
		create and return a new `Webapp` instance
		'''
		return Webapp.objects.create(**validated_data)

	def update(self, instance, validated_data):

		'''
		update and return an existing `Webapp` instance
		'''

		instance.title = validated_data.get('title', instance.title)
		instance.code = validated_data.get('code',instance.code)
		instance.linenos = validated_data.get('linenos',instance.linenos)
		instance.language = validated_data.get('language',instance.language)
		instance.style = validated_data.get('style',instance.style)
		instance.save()
		return instance