from django.shortcuts import render
from django.http import HttpResponse
from django.http import Http404
from rest_framework import generics
from webapp.models import Webapp
from webapp.serializers import WebappSerializer
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from webapp.models import Article



# class JSONResponse(HttpResponse):

#     """
#     An HttpResponse that renders its content into JSON.
#     """

#     def __init__(self, data, **kwargs):
#         content = JSONRenderer().render(data)
#         kwargs['content_type'] = 'application/json'
#         super(JSONResponse, self).__init__(content, **kwargs)

# test api
class WebapiList(generics.ListCreateAPIView):
    queryset = Webapp.objects.all()
    serializer_class = WebappSerializer

class WebapiDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Webapp.objects.all()
    serializer_class = WebappSerializer


def home(request):
    return render(request,'home.html')

'''
'''
def home(request):
	posts = Article.objects.all() # get all article objects
	paginator = Paginator(posts,9) # 每页显示9个
	page = request.GET.get('page')
	try:
		post_list = paginator.page(page)
	except PageNotAnInteger:
		post_list = paginator.page(1)
	except EmptyPage:
		post_list = paginator.paginator(paginator.num_pages)
	return render(request,'home.html',{'post_list': post_list})

def detail(request, id):
	try:
		post = Article.objects.get(id=str(id))
	except Article.DoesNotExist:
		raise Http404
	return render(request, 'post.html',{'post':post})