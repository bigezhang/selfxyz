# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0002_auto_20151110_1423'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webapp',
            name='code',
            field=models.TextField(blank=True),
        ),
    ]
