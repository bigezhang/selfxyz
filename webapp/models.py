# -*-coding: utf-8 -*-

from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
from django.core.urlresolvers import reverse

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())

# test rest api

class Webapp(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    code = models.TextField(blank=True)
    linenos = models.BooleanField(default=False)
    language = models.CharField(choices=LANGUAGE_CHOICES, default='python', max_length=100)
    style = models.CharField(choices=STYLE_CHOICES, default='friendly', max_length=100)

    class Meta:
        ordering = ('created',)


class Article(models.Model):
    title = models.CharField(max_length=100)
    desc = models.CharField(max_length=100)
    url = models.CharField(max_length=150)
    created = models.DateTimeField(auto_now_add=True)

    #获取URL ,并转换成url 的表示格式
    def get_absolute_url(self):
    	path = reverse('detail',kwargs={'id':self.id})
    	return "http://127.0.0.1:8000%s" % path

    def __str__(self):
    	return self.title

    class Meta:
    	ordering = ['-created']
